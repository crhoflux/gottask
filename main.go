package main

import (
  "bufio"
  "fmt"
  "os"
  "os/exec"
  "regexp"
  "strconv"
  "strings"

  cursor "github.com/ahmetalpbalkan/go-cursor"
  "github.com/ttacon/chalk"
  "gopkg.in/yaml.v2"
)

// VERSION of the programm
const VERSION string = "0.2.0"

// filePath of .yaml which stores all tasks
const filePath string = "tasks.yaml" // "~/.gottask/tasks.yaml"

// Task struct
type Task struct {
  Name        string `yaml:"name"`
  Short       string `yaml:"short"`
  Commands    string `yaml:"commands"`
  Interactive bool   `yaml:"interactive"`
  ID          uint16 `yaml:"id"`
}

func main() {
  fmt.Print(cursor.Show())

  error := chalk.Red.NewStyle().
    WithBackground(chalk.Black).
    WithTextStyle(chalk.Bold).
    Style

  reader := bufio.NewReader(os.Stdin)
  writer := bufio.NewWriter(os.Stdout)

  var file *os.File

  file, err := os.OpenFile(filePath, os.O_RDONLY|os.O_CREATE, 0744)
  check(err)
  defer file.Close()

  args := os.Args

  // wenn mehr als ein argument hinter dem Programmnamen steht
  if len(args) > 1 {

    var argument = string(args[1])

    switch argument {
    case "gottask":
      createNewTask(file, reader, writer, &args)
      return

    case "help":
      printUsage(writer)
    }

    matched, err := regexp.MatchString(`^[0-9]{4,5}$`, argument)
    check(err)

    if matched {
      IDu64, err := strconv.ParseUint(argument, 10, 16)
      check(err)
      IDu16 := uint16(IDu64)
      parseIDs(file, reader, writer, &IDu16)

    } else {
      matched := parseShorts(file, reader, writer, &argument)

      if !matched {
        printUsage(writer)
      }
    }

  } else {
    printUsage(writer)
  }

}

func createNewTask(file *os.File, reader *bufio.Reader, writer *bufio.Writer, args *[]string) {

  var newTask Task

  question1 := "Name your Task (enter):"
  newTask.Name = strings.Replace(prompt(writer, reader, question1), "\n", "", -1)

  question2 := "What is the short command of your Task? (enter):"
  newTask.Short = strings.Replace(prompt(writer, reader, question2), "\n", "", -1)

  // valid := false
  // for !valid {
  //   question3 := "Is it interactive? (y/N):"
  //   answer3 := prompt(*writer, *reader, question3)
  //   answer3 = strings.Replace(answer3, "\n", "", -1)
  //
  //   for strings.Contains(answer3, " ") {
  //     answer3 = strings.Replace(answer3, " ", "", -1)
  //   }
  //
  //   switch answer3 {
  //   case "y", "Y":
  //     newTask.Interactive = true
  //     valid = true
  //   case "n","N", "":
  //     newTask.Interactive = false
  //     valid = true
  //   default:
  //     fmt.Fprintln(writer, "\""+answer3+"\"", " is not a valid answer, try again:")
  //     writer.Flush()
  //     valid = false
  //   }
  // }

  question4 := "Which commands shall be executed (bash):\n"
  newTask.Commands = strings.Replace(prompt(writer, reader, question4), "\n", "", -1)

  fmt.Fprintln(writer, "Your Task:", newTask)
  writer.Flush()

  // store it in yaml File
  storeNewTaskInYamlFile(file, &newTask)

}

func parseIDs(file *os.File, reader *bufio.Reader, writer *bufio.Writer, possibleID *uint16) {
  error := chalk.Red.NewStyle().
    WithBackground(chalk.Black).
    WithTextStyle(chalk.Bold).
    Style

  var (
    tasksFromYaml []Task
    isStoredTask  bool
    matchedIndex  int
  )

  loadYaml(file, &tasksFromYaml)

  for index, taskX := range tasksFromYaml {
    if taskX.ID == *possibleID {
      isStoredTask = true
      matchedIndex = index
    }
  }

  if isStoredTask {
    execTask(&tasksFromYaml[matchedIndex], reader, writer)
  } else {
    printwf(writer, fmt.Sprintln(error("ERR"), "No Task with ID %d found"), *possibleID)
  }

}

func parseShorts(file *os.File, reader *bufio.Reader, writer *bufio.Writer, possibleShort *string) bool {
  var matched bool

  return matched
}

func execTask(matchedTask *Task, reader *bufio.Reader, writer *bufio.Writer) {

  separated := strings.Split(matchedTask.Commands, " ")
  cmd := exec.Command(separated[0], separated[1:]...)
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  err := cmd.Run()
  check(err)
}

// utils

func prompt(w *bufio.Writer, r *bufio.Reader, question ...interface{}) string {
  fmt.Fprint(w, question...)
  w.Flush()
  answer, _ := r.ReadString('\n')

  return answer
}

func printwf(w *bufio.Writer, msg string, vars ...interface{}) {
  fmt.Fprint(w, fmt.Sprintf(msg, vars...))
  w.Flush()
}

func loadYaml(file *os.File, tasksFromYaml *[]Task) *[]Task {
  fileinfo, err := file.Stat()
  check(err)

  filesize := fileinfo.Size()
  buffer := make([]byte, filesize)

  n, err := file.Read(buffer)
  _ = n
  check(err)

  // (yaml File, output)
  err = yaml.Unmarshal(buffer, &tasksFromYaml)
  check(err)

  return tasksFromYaml
}

func storeNewTaskInYamlFile(file *os.File, newTask *Task) {
  var tasksFromYaml []Task

  loadYaml(file, &tasksFromYaml)

  // set unique id
  newTask.ID = generateID(&tasksFromYaml)

  allTasks := append(tasksFromYaml, *newTask)

  out, err := yaml.Marshal(&allTasks)
  check(err)

  var newFilePath = "tasks_new.yaml"

  newFile, err := os.OpenFile(newFilePath, os.O_WRONLY|os.O_CREATE, 0744)
  check(err)
  defer newFile.Close()

  n, err := newFile.Write(out)
  _ = n
  check(err)

  // delete old file
  os.Remove(filePath)
  // rename new file
  os.Rename(newFilePath, filePath)

}

func filterInput(input string) bool {
  return strings.Contains(input, " ")
}

func generateID(tasksFromYaml *[]Task) uint16 {
  const id uint16 = 12212
  return id
}

func printUsage(writer *bufio.Writer) {
  usage :=
    `GotTask Version %s

  Usage:
    task <command | call>

    Commands:
      gottask [<name>]  create a new GotTask task
      help              prints this page

    Calls:
      <taskID>          executes task with ID "taskID"
      <short>           executes task with short "short"
`
  printwf(writer, usage, VERSION)
  writer.Flush()
}

func check(err error) {
  if err != nil {
    panic(err)
  }
}
